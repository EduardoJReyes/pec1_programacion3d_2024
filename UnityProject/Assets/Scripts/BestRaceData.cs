using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu]
public class BestRaceData : ScriptableObject
{
    //BestRaceData is used to store the best information given from NewRaceData

    public float recordFrequency = 100; //Important: This frequency should be the same as newRaceData.  (The higher the smoother, but larger data)

    public List<float> timeStamp;
    public List<Vector3> position;
    public List<Quaternion> rotation;
    public float getRaceTime => timeStamp.Count > 0 ? timeStamp.LastOrDefault() - timeStamp.FirstOrDefault() : float.MaxValue; //Difference between the first and last time stamp if it has data
    public bool hasData => timeStamp.Count > 0;
    public void ResetData()
    {
        timeStamp.Clear();
        position.Clear();
        rotation.Clear();
    }

}
