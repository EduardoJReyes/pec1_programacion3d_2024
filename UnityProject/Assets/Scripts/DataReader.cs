using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader : MonoBehaviour
{
    //This script will be used by the ghostcar in order to use the data to move through the circuit

    public BestRaceData bestRaceData;
    public NewRaceData newRaceData;
    public GameManager gameManager;

    //This and index2 will help to use the data to use linear interpolation between the values.
    private int index1; 
    private int index2;

    private void GetIndex()
    {
        //We look through the time stamp stored value of the ghost, if the current value is  equal to a store time stamp we set both indexes to at that value since we don't need lerp. If it is not stored, we will take the index1 and 2 to create the interpolation. 
        for (int i = 0; i < bestRaceData.timeStamp.Count - 2; i++)
        {
            if (bestRaceData.timeStamp[i] == gameManager.raceTime)
            {
                index1 = i;
                index2 = i;
                return;
            }
            else if (bestRaceData.timeStamp[i] < gameManager.raceTime & gameManager.raceTime < bestRaceData.timeStamp[i + 1])
            {
                index1 = i;
                index2 = i + 1;
                return;
            }
        }
        index1 = bestRaceData.timeStamp.Count - 1;
        index2 = bestRaceData.timeStamp.Count - 1;
    }

    private void SetTransform()
    {
        //Math to do the lerp in order to simulate the movement fluidly.
        if (index1 == index2)
        {
            this.transform.position = bestRaceData.position[index1];
            this.transform.rotation = bestRaceData.rotation[index1];
        }
        else
        {
            float interpolationFactor = (gameManager.raceTime - bestRaceData.timeStamp[index1]) / (bestRaceData.timeStamp[index2] - bestRaceData.timeStamp[index1]);

            this.transform.position = Vector3.Slerp(bestRaceData.position[index1], bestRaceData.position[index2], interpolationFactor);
            this.transform.rotation = Quaternion.Slerp(bestRaceData.rotation[index1], bestRaceData.rotation[index2], interpolationFactor);
        }
    }

    public void ResetIndexes()
    {
        index1 = 0;
        index2 = 0;
    }

    public void CopyNewData()
    {
        //This copies the data over the bestRaceData if the newRaceData is better (Faster) than the last bestRaceData.

        if (newRaceData.getRaceTime < bestRaceData.getRaceTime)
        {
            bestRaceData.timeStamp.Clear();
            bestRaceData.position.Clear();
            bestRaceData.rotation.Clear();

            bestRaceData.timeStamp.AddRange(newRaceData.newTimeStamp);
            bestRaceData.position.AddRange(newRaceData.newPosition);
            bestRaceData.rotation.AddRange(newRaceData.newRotation);

            newRaceData.newTimeStamp.Clear();
            newRaceData.newPosition.Clear();
            newRaceData.newRotation.Clear();

        }
    }

    private void FixedUpdate()
    {
        if (bestRaceData.hasData)
        {
            GetIndex();
            SetTransform();
        }
    }

}


