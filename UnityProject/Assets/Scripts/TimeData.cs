using UnityEngine;

[CreateAssetMenu]
public class TimeData : ScriptableObject
{
    //This is a pile used to store the three previous last best times of the race

    public float bestTime1 = 0f;
    public float bestTime2 = 0f;
    public float bestTime3 = 0f;

    public void UpdateBestTimes(float raceTime)
    {
        if (raceTime < bestTime1 || bestTime1 == 0f)
        {
            bestTime3 = bestTime2;
            bestTime2 = bestTime1;
            bestTime1 = raceTime;
        }
        else if (raceTime < bestTime2 || bestTime2 == 0f)
        {
            bestTime3 = bestTime2;
            bestTime2 = raceTime;
        }
        else if (raceTime < bestTime3 || bestTime3 == 0f)
        {
            bestTime3 = raceTime;
        }
    }

}
