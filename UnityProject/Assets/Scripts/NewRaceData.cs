using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu]
public class NewRaceData : ScriptableObject
{
    //NewRaceData is used to store the data of the current run.

    public float recordFrequency = 100; //Important: This frequency should be the same as BestRaceData.  (The higher the smoother, but larger data)
    public bool isRecord; //When true, it will be in record mode, recording the character's position and rotation.

    public List<float> newTimeStamp;
    public List<Vector3> newPosition;
    public List<Quaternion> newRotation;

    public float getRaceTime
    {
        get
        {
            return newTimeStamp.LastOrDefault() - newTimeStamp.FirstOrDefault();
        }
    } //This gets the first and last Race times

    public void ResetData()
    {
        newTimeStamp.Clear();
        newPosition.Clear();
        newRotation.Clear();

    }
    public void AddNewData(Vector3 pos, Quaternion rot)
    {
        newTimeStamp.Add(Time.time);
        newPosition.Add(pos);
        newRotation.Add(rot);
    }

}
