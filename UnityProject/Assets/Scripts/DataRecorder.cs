using UnityEngine;

public class DataRecorder : MonoBehaviour
{
    //This script records the data and stores it in newRaceData

    public BestRaceData bestRaceData;
    public NewRaceData newRaceData;
    public GameManager gameManager;

    private void Awake()
    {
        if (newRaceData.isRecord)
        {
            newRaceData.ResetData();
        }
    }

    private void FixedUpdate()
    {
        if (newRaceData.isRecord)
        {
            newRaceData.newTimeStamp.Add(gameManager.raceTime);
            newRaceData.newPosition.Add(this.transform.position);
            newRaceData.newRotation.Add(this.transform.rotation);
        }
    }


}
