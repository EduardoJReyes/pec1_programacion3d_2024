using UnityEngine;

public class LapCount : MonoBehaviour
{
    //This script is used for counting the laps of the car.
    //In order to avoid the player moving back and forth and adding laps to the counter, it will work with the LapCountReset to deactivate itself once passed and re-activated with the mentioned script.

    public int lapCount = 0;
    public bool canCountLaps = true; 

    private void Start()
    {
        Debug.Log("Lap " + lapCount);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (canCountLaps && other.CompareTag("PlayerCar"))
        {
            lapCount++;
            //Debug.Log("Lap " + lapCount);

            canCountLaps = false; 
        }
    }

}
