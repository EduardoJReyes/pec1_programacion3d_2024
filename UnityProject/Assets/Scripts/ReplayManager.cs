using System.Collections;
using UnityEngine;
using Cinemachine;

public class ReplayManager : MonoBehaviour
{
    public NewRaceData newRaceData;
    public GameManager gameManager;
    public CinemachineVirtualCamera virtualCamera;

    public GameObject replayCarCameraTarget;
    public GameObject mainCar;

    public bool isActive = false;
    public bool CinematicMode = true; //This variable refers to either put the camera on set places during the replay or behind the car.
    public float cameraSwitchDistance = 500f;

    private void ChangeCameraPosition()
    {
        // This checks if the replay car is colliding with triggers tagged "ReplayCameraPosition", then change the position of the camera to said colliders

        Collider[] colliders = Physics.OverlapSphere(transform.position, cameraSwitchDistance);
        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("ReplayCameraPosition"))
            {
                virtualCamera.transform.position = collider.transform.position;
                virtualCamera.transform.LookAt(replayCarCameraTarget.transform);
                break;
            }
        }
    }

    public void StartReplay()
    {
        //This is used in the GameManager in order to activate the replay

        if (isActive)
        {
            mainCar.SetActive(false);
            StartCoroutine(ReplayCoroutine());
        }
    }

    private void Update()
    {
        //This Toggles the Cinematic Mode with the Q button

        if (Input.GetKeyDown(KeyCode.Q) && isActive)
        {
            CinematicMode = !CinematicMode;

            if (CinematicMode)
            {
                virtualCamera.Follow = null;
                virtualCamera.LookAt = replayCarCameraTarget.transform;
            }
            else
            {
                virtualCamera.Follow = replayCarCameraTarget.transform;
                virtualCamera.LookAt = replayCarCameraTarget.transform;
            }

        }
    }

    IEnumerator ReplayCoroutine()
    {
        //Here is how the replay itself is managed

        for (int i = 0; i < newRaceData.newTimeStamp.Count - 1; i++)
        {
            float startTime = Time.time;
            float endTime = startTime + (newRaceData.newTimeStamp[i + 1] - newRaceData.newTimeStamp[i]);

            while (Time.time < endTime)
            {
                // We calculate the interpolation factor based on the current time relative to start and end time
                float interpolationFactor = Mathf.Clamp01((Time.time - startTime) / (endTime - startTime));

                // And then update position and rotation based on interpolation factor
                transform.position = Vector3.Lerp(newRaceData.newPosition[i], newRaceData.newPosition[i + 1], interpolationFactor);
                transform.rotation = Quaternion.Slerp(newRaceData.newRotation[i], newRaceData.newRotation[i + 1], interpolationFactor);

                // Then finally changes camera position
                if (CinematicMode)
                {
                    ChangeCameraPosition();
                }

                yield return null;
            }
        }

        // Reactivates buttons  and texts when the replay has finished
        gameManager.playAgain.gameObject.SetActive(true);
        gameManager.replay.gameObject.SetActive(true);
        gameManager.countdownText.gameObject.SetActive(true);
        gameManager.bestTimesPanel.gameObject.SetActive(true);
        gameManager.bestTimes.enabled = true;
        gameManager.pressQ.enabled = false;
        
        isActive = false;
    }

}
