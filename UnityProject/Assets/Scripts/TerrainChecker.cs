using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainChecker : MonoBehaviour
{

    //This script is used by the WheelController, to lower the velocity of the car IF one of the wheels is touching the Terrain

    public WheelCollider wheelCollider;
    public WheelController wheelController;

    public bool IsOffRoad()
    {
        wheelCollider.GetGroundHit(out WheelHit hit);
        if (hit.collider != null)
        {
            return hit.collider.gameObject.name == "Terrain";
        }
        else return false;
    }
}

