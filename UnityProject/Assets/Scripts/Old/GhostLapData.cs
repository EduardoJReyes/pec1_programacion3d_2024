using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GhostLapData : ScriptableObject
{
    public List<Vector3> carPositions;
    public List<Quaternion> carRotations;

    public void AddNewData(Transform transform)
    {
        carPositions.Add(transform.position);
        carRotations.Add(transform.rotation);
    }

    public void GetDataAt(int sample, out Vector3 position, out Quaternion rotation)
    {
        position = carPositions[sample];
        rotation = carRotations[sample];
    }

    public void Reset()
    {
        carPositions.Clear();
        carRotations.Clear();
    }
}
