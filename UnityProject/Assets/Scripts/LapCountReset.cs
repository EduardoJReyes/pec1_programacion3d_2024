using UnityEngine;

public class LapCountReset : MonoBehaviour
{
    //This script is used to reactivate the lapCountScript. 

    public LapCount lapCountScript;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerCar"))
        {
            if (lapCountScript != null)
            {
                lapCountScript.enabled = true;
                lapCountScript.canCountLaps = true;
            }
        }
    }
}
