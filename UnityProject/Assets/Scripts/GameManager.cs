using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;

public class GameManager : MonoBehaviour
{
    //As the name implies, the race and whole match will be managed through this script.

    public GameObject HUD;
    public GameObject bestTimesPanel;
    public GameObject ghostCar;
    public GameObject lapCounterCollider; 
    public GameObject mainCar; 

    public Text countdownText;
    public Text timerText;
    public Text pressQ;
    public Text bestTimes;
    public Text currentLapText;

    public float timerTextNumber;
    private float initialTime;
    public float raceTime => Time.time - initialTime;

    public Button playAgain;
    public Button replay;

    public TimeData timeData;
    public CinemachineVirtualCamera virtualCamera; 
    public BestRaceData bestRaceData;
    public NewRaceData newRaceData;
    public DataReader dataReader;
    public ReplayManager replayManager;
    private WheelController wheelController;
    private Scene currentScene;

    public void UpdateBestTimesDisplay()
    {
        // This updates the three last best records

        string bestTimesString = "PREVIOUS RECORDS:\n";
        if (timeData.bestTime1 > 0)
            bestTimesString += "1. " + timeData.bestTime1.ToString("F1") + " seconds\n";
        if (timeData.bestTime2 > 0)
            bestTimesString += "2. " + timeData.bestTime2.ToString("F1") + " seconds\n";
        if (timeData.bestTime3 > 0)
            bestTimesString += "3. " + timeData.bestTime3.ToString("F1") + " seconds";

        bestTimes.text = bestTimesString;

    }

    public void WatchRace()
    {
        //This method is used in order to activate the replay.
       
        bestTimes.enabled = false;
        pressQ.enabled = true;
        countdownText.enabled = false;

        HUD.SetActive(false);
        playAgain.gameObject.SetActive(false);
        replay.gameObject.SetActive(false);
        ghostCar.SetActive(false);
        bestTimesPanel.SetActive(false);

        replayManager.isActive = true;

        replayManager.StartReplay();
    }

    private void ShowButtons()
    {
        playAgain.gameObject.SetActive(true);
        replay.gameObject.SetActive(true);
        bestTimesPanel.SetActive(true);
        bestTimes.enabled = true;
    }

    public void RestartScene()
    {
        dataReader.CopyNewData();
        newRaceData.ResetData();
        SceneManager.LoadScene(currentScene.name);
    }

    void Start()
    {

        HUD.SetActive(false);
        bestTimesPanel.SetActive(false);
        ghostCar.SetActive(false);
        playAgain.gameObject.SetActive(false);
        replay.gameObject.SetActive(false);

        pressQ.enabled = false;
        bestTimes.enabled = false;

        newRaceData.isRecord = false;

        wheelController = mainCar.GetComponent<WheelController>();
        StartCoroutine(StartCountdown());

        currentScene = SceneManager.GetActiveScene();
    }

    IEnumerator StartCountdown()
    {
        //The game starts after the 3,2 and 1. Then the timer will count and the laps will be updated.

        wheelController.enabled = false;

        countdownText.text = "3";
        yield return new WaitForSeconds(1f);
        countdownText.text = "2";
        yield return new WaitForSeconds(1f);
        countdownText.text = "1";
        yield return new WaitForSeconds(1f);
        countdownText.text = "Go!";

        initialTime = Time.time;

        ghostCar.SetActive(true);
        HUD.SetActive(true);
        newRaceData.isRecord = true;

        countdownText.enabled = false;
        bestTimes.enabled = false;
        wheelController.enabled = true;

        while (true)
        {
            timerText.text = "Current Time: " + raceTime.ToString("F1");

            currentLapText.text = "Current Lap: " + lapCounterCollider.GetComponent<LapCount>().lapCount + "/3";

            if (lapCounterCollider.GetComponent<LapCount>().lapCount == 3) //This can be changed to the amount of max laps necessary.
            {
                virtualCamera.Follow = null;
                virtualCamera.LookAt = null;

                newRaceData.isRecord = false;

                timeData.UpdateBestTimes(raceTime);
                UpdateBestTimesDisplay();

                countdownText.text = "Race finished!";
                countdownText.enabled = true;

                wheelController.enabled = false;
                pressQ.enabled = false;

                yield return new WaitForSeconds(2f);

                ShowButtons();
                yield break;
            }

            yield return null;
        }


        yield return null;
    }
}





