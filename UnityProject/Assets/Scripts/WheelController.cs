using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelController : MonoBehaviour
{
    //This script is the car controller, where the wheels will acelerate, turn and brake.

    [SerializeField] WheelCollider frontRight;
    [SerializeField] WheelCollider frontLeft;
    [SerializeField] WheelCollider backRight;
    [SerializeField] WheelCollider backLeft;

    [SerializeField] Transform frontRightTransform;
    [SerializeField] Transform frontLeftTransform;
    [SerializeField] Transform backLeftTransform;
    [SerializeField] Transform backRightTransform;
    [SerializeField] TerrainChecker[] checkers;

    public Rigidbody carRigidbody;
    public Vector3 centerOfMassOffset = new Vector3(0f, -0.5f, 0f);

    public float acceleration = 1000f;
    public float brakingForce = 3000f;
    public float maxTurnAngle = 30f;
    public float wheelFriction = 1f;
    public float carWeight = 1500f;

    private float currentAcceleration = 0f;
    private float currentBrakeForce = 0f;
    private float currentTurnAngle = 0f;

    public float maxSpeed = 100f;


    void UpdateWheel(WheelCollider col, Transform trans)
    {

        //This updates the rotation and position of the wheel

        Vector3 position;
        Quaternion rotation;
        col.GetWorldPose(out position, out rotation);

        trans.position = position;
        trans.rotation = rotation;
    }

    private void SetWheelFriction(WheelCollider wheel, float friction)
    {
        //This sets the friction of the wheels based on a variable.

        WheelFrictionCurve forwardFriction = wheel.forwardFriction;
        forwardFriction.stiffness = friction;
        wheel.forwardFriction = forwardFriction;

        WheelFrictionCurve sidewaysFriction = wheel.sidewaysFriction;
        sidewaysFriction.stiffness = friction;
        wheel.sidewaysFriction = sidewaysFriction;
    }

    private void SetCarWeight(Rigidbody rigidbody, float weight)
    {
        //This changes the weight of the car based on a variable
        rigidbody.mass = weight;
    }

    void Start()
    {
        //We're doing this in order to make the car have a lower gravity center, so it's harder to turn over.
        carRigidbody = GetComponent<Rigidbody>();
        SetCarWeight(carRigidbody, carWeight);
        carRigidbody.centerOfMass = centerOfMassOffset;

        SetWheelFriction(frontRight, wheelFriction);
        SetWheelFriction(frontLeft, wheelFriction);
        SetWheelFriction(backRight, wheelFriction);
        SetWheelFriction(backLeft, wheelFriction);
    }

    private void FixedUpdate()
    {
        currentAcceleration = acceleration * Input.GetAxis("Vertical");

        if (Input.GetKey(KeyCode.Space))
            currentBrakeForce = brakingForce;
        else
            currentBrakeForce = 0f;

        frontRight.motorTorque = currentAcceleration;
        frontLeft.motorTorque = currentAcceleration;
        backRight.motorTorque = currentAcceleration;
        backLeft.motorTorque = currentAcceleration;

        frontRight.brakeTorque = currentBrakeForce;
        frontLeft.brakeTorque = currentBrakeForce;
        backRight.brakeTorque = currentBrakeForce;
        backLeft.brakeTorque = currentBrakeForce;

        currentTurnAngle = maxTurnAngle * Input.GetAxis("Horizontal");
        frontLeft.steerAngle = currentTurnAngle;
        frontRight.steerAngle = currentTurnAngle;

        UpdateWheel(frontRight, frontRightTransform);
        UpdateWheel(frontLeft, frontLeftTransform);
        UpdateWheel(backLeft, backLeftTransform);
        UpdateWheel(backRight, backRightTransform);

        bool isOffRoad = false;

        for (int i = 0; i < checkers.Length; i++)
        {
            isOffRoad = isOffRoad || checkers[i].IsOffRoad();
        }
        maxSpeed = isOffRoad ? 10 : 100; // 10 being the slowed speed, 100 is the normal one

        if (carRigidbody.velocity.magnitude > maxSpeed)
        {
            carRigidbody.velocity = carRigidbody.velocity.normalized * maxSpeed;
        }
    }

}
