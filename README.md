**PEC1_Programacion3D_2024**
----------------------------------------------------------------------------------------------------------------------------------

**CONTROLS:**
- W to move forward.
- A/D to turn.
- SPACE to brake.

**[LINK TO PLAY THE GAME](https://drive.google.com/file/d/1ieUtD0k5uwZURZg5G_BJfAMU16DoQSlH/view?usp=sharing)** 
_The game can be played by double clicking the .exe, no need to install._

**[LINK TO GAMEPLAY](https://youtu.be/95aeklu_b8E)** 


----------------------------------------------------------------------------------------------------

**IMPLEMENTATION SUMMARY:**
1. The game features **one circuit** that is located partly in a **city enviroment and in mountain roads**, containing two shorcuts, all in a consistent low poly retro style. 

![IMAGE_DESCRIPTION](https://gitlab.com/EduardoJReyes/pec1_programacion3d_2024/-/raw/main/Captures/CityRoad.png?ref_type=heads)

2. A **car that can be controlled** and its **speed modified by the type of terrain** it is currently being moved _(WheelController.cs and TerrainChecker.cs)_. 
    - The player car has been provided with a script that controls the acceleration, maximum angle of turning, friction and braking of the four wheels with wheel colliders, as well as the weight and maximum speed of the car. 
    - In order to avoid possible issues when flipping, the car's weight center has been lowered, offering an almost automatic fliping when ot has been rolled over.  

![IMAGE_DESCRIPTION](https://gitlab.com/EduardoJReyes/pec1_programacion3d_2024/-/raw/main/Captures/WheelControllerScript.png?ref_type=heads)

3. A race that lasts **three laps** _(LapCount.cs and LapCountReset.cs)._ 
    - The script uses a simple logic to count the laps with two script where, when traversing the trigger on the LapCount, will count one lap and deactivate the script itself then get reativated once the trigger of the LapCountReset has been activated. This is done with the objective to avoid the player to have laps counted when moving back and forth in the starting line. 
    
    - This could be expanded with more LapCountResets if the circuit length requires it. 

![IMAGE_DESCRIPTION](https://gitlab.com/EduardoJReyes/pec1_programacion3d_2024/-/raw/main/Captures/MountainRoad.png?ref_type=heads)
   

4. **An active recorder** that will record the **player's movement** in order to **use its data in a Ghost car** in the next match. _(DataRecorder.cs, DataReader.cs and NewRaceData.cs, BestRaceData.cs)_

    - At the end of each race, a comparison between the new data from the player's race and the current ghost car (Except in the first race) will be done in order to find out if the new race data is better, in order to copy it into the data the Ghost car will use in the next race.    

    - The DataRecorder will record and store data related to the position, rotation and time through the race and store it in NewRaceData. At the end NewRaceData will be compared with BestRaceData (If there is any). If the NewRaceData is better than the previous one (Meaning that the time is faster) this information will be copied in BestRaceData, which, alongside the DataReader, will use this information to move the Ghost Car. NewRaceData will be cleared always at the beginning of the scene.

![IMAGE_DESCRIPTION](https://gitlab.com/EduardoJReyes/pec1_programacion3d_2024/-/raw/main/Captures/DataRecorder.png?ref_type=heads)

5. The possibility to see the **replay of the race** with two different perspectives: The **normal camera** behind the car, and a group of **cinematic cameras** shared through the level. All of them using **CineMachine**. _(ReplayManager.cs and NewRaceData.cs)_

    - The information will be handled in a similar manor than the Ghost car, but directly with the most recent NewRaceData.

![IMAGE_DESCRIPTION](https://gitlab.com/EduardoJReyes/pec1_programacion3d_2024/-/raw/main/Captures/ReplayCamera.PNG?ref_type=heads)

5. **Time** of the current race is **displayed through all the gameplay**, as well as the current lap. Once the race ends, a **list of the top three best times are displayed** and **it can be updated as the player plays more** races and improves their timing._(GameManager.cs, TimeData.cs)_

    -  In the GameManager, is where the main events are controlled, specifically, when the match starts, when it ends and which information displayed. This is done with an IEnumerator and the activation / deactivation of HUD elements, car and options at the end, which lets the player decide if they want to see the replay of the race or play again.

![IMAGE_DESCRIPTION](https://gitlab.com/EduardoJReyes/pec1_programacion3d_2024/-/raw/main/Captures/EndScreen.png?ref_type=heads)


Note that the **NewRaceData and BestRaceData info is always stored persistently** in-game with scriptable objects.

----------------------------------------------------------------------------------------------------

**FREE RESOURCES USED:**
- Cinemachine by Unity
- PostProcessing by Unity
- Easy3DRoads Free v3 by AndaSoft
- Environment Track Lowpoly: Cartoon Props Mobile Free by BE DRILL ENTER
- CITY package by 255 pixel studios
- Low Poly Road Pack by Broken Vector
- Low-Poly Simple Nature Pack by JustCreate
- Tiny Low Poly Cars by David Jalbert
